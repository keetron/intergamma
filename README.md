# Intergamma code assignment

## What are we dealing with?

This is a simple showcase of a Spring boot service using Kotlin as a language and H2 as storage. It will load some base
data from `data.sql` in the resources folder.

### How to run this service

This service needs Java 17 JDK and Kotlin 1.6 available on your machine.

You can run the service in intellij, by right-clicking `IntergammaCodeAssessmentApplication.kt` and choosing `Run`.

Would you prefer a command line, run `./gradlew bootRun` on the command line.

The service can be called on `localhost:8080`, the h2 console is enabled on `http://localhost:8080/h2-console/` because
I think that is pretty cool to have. In a production environment I would NOT use it, for obvious reasons.

### How to test this service

You can run the tests in intellij, by right-clicking `IntergammaCodeAssessmentApplicationTest.kt` and choosing `Run`. I
used a very straightforward MockMvc setup with the Kotlin DSL.

For reasons that are beyond me on the Sunday afternoon, commandline Gradle cannot find the tests. If I had more time,
I'd fix it as that should work of course.

### What is missing?

The following is very much missing from this repository, a list of things I would normally build but excluded for the
sake of time:

* Unit tests on anything, normally I would work test driven and build per component using junit5 and mockk for service
  classes and mockmvc with mockk for controller configurations. But this service is really small AND there is a time
  constraint so I used just the integration test. Let's talk about preferences.
* Update endpoint to change the description of an article, handle out of stock articles, doing stuff with shops
* actual error handling, an error will now throw a 500 and barf out a stacktrace (if you're lucky)
* running the tests from the command line
* I would have loved to mess around with MongoDb for this but I wanted a working solution in 24h and hibernate is what I
  can do from the top of my head at this time.
* I had some doubts about the requirement `reserve for a maximum of 5 minutes`, does it imply a configurable amount? I
  went with fixed 5 minutes for now, making it configurable is trivial. In day to day work, I'd ask product what the
  usecase is.
* Everything considered, the data model is clunky as well as the functionality of making reservations. In day to day
  work, I'd talk to my colleagues to see if anyone has a better idea or if we'd build this for now.
* The assignment talks about nice to haves such as a GUI, API docs & auth. I am open to whatever cool plugins would make
  that instant without having to fight some config I've never seen before for hours.
* I considered putting in detekt but not everyone thinks that is a nice thing to have ;-)

