package nl.tnca.intergamma

import nl.tnca.intergamma.article.ArticleRepository
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import kotlin.test.assertEquals


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class IntergammaCodeAssessmentApplicationTest {

    @Autowired
    lateinit var articleRepository: ArticleRepository

    @Autowired
    lateinit var mvc: MockMvc

    @Test
    fun getArticle_getByArticleNumber_succeeds() {
        mvc.get("/article/64fe62e8-6c35-4d63-8082-4c6f9df46423") {
            accept = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("size()", equalTo(3))
            jsonPath("$[0].number", equalTo("64fe62e8-6c35-4d63-8082-4c6f9df46423"))
            jsonPath("$[0].description", equalTo("hammer"))
            jsonPath("$[0].shopId", equalTo(1))
        }
        println("This has ran")
    }

    @Test
    fun getArticleStockByShop_getByShopId_succeeds() {
        mvc.get("/article/64fe62e8-6c35-4d63-8082-4c6f9df46423") {
            param("shopId", "1")
            contentType = MediaType.APPLICATION_JSON
            accept = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("size()", equalTo(1))
            jsonPath("$[0].number", equalTo("64fe62e8-6c35-4d63-8082-4c6f9df46423"))
            jsonPath("$[0].description", equalTo("hammer"))
            jsonPath("$[0].shopId", equalTo(1))
        }
    }

    @Test
    fun saveNewArticleStock_postArticle_succeeds() {
        mvc.post("/article") {
            contentType = MediaType.APPLICATION_JSON
            content = "{\"description\":\"screwDriver\",\"shopId\":1}"
            accept = MediaType.APPLICATION_JSON
            param("amount", "5")
        }.andExpect {
            status { isOk() }
            content { contentType(MediaType.APPLICATION_JSON) }
            jsonPath("size()", equalTo(5))
            jsonPath("$[0].description", equalTo("screwDriver"))
        }
    }

    @Test
    fun reserveArticleStock_reserveSingleItemByTime_succeeds() {
        mvc.put("/article/64fe62e8-6c35-4d63-8082-4c6f9df46424/reserve") {
            accept = MediaType.APPLICATION_JSON
            param("amount", "1")
            param("shopId", "1")
        }.andExpect {
            status { isOk() }
            // when trying to do this properly, I arrived in date/time formatting hell, I'll gladly explain
            jsonPath("$.reservedUntil", notNullValue())
        }
    }

    @Test
    fun removeArticleStock_deleteArticleFromStore_succeeds() {
        assertEquals(1, articleRepository.findByNumber("a4fe62e8-6c35-4d63-8082-4c6f9df46424").size)


        mvc.delete("/article/a4fe62e8-6c35-4d63-8082-4c6f9df46424") {
            param("shopId", "3")
        }.andExpect {
            status { isEqualTo(204) }
        }

        assertEquals(0, articleRepository.findByNumber("a4fe62e8-6c35-4d63-8082-4c6f9df46424").size)
    }

    @Test
    fun removeArticleStock_deleteAllArticlesFromStore_succeeds() {
    }

}
