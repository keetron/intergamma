package nl.tnca.intergamma.article

import nl.tnca.intergamma.article.domain.Article
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/article")
class ArticleController(
    val articleService: ArticleService
) {

    @GetMapping("/{articleNumber}")
    fun getArticle(
        @PathVariable("articleNumber") number: String,
        @RequestParam("shopId", required = false) shopId: Long?,
        @RequestParam("available", defaultValue = "true") available: Boolean
    ): List<Article> {
        return articleService.getArticlesByNumberAndShop(number, shopId, available)
    }

    @PostMapping(consumes = ["application/json"])
    fun postArticle(@RequestBody articles: Article, @RequestParam amount: Int): List<Article> {
        return articleService.saveNewArticles(amount, articles)
    }

    @PutMapping("/{articleNumber}/reserve")
    fun updateArticle(@PathVariable("articleNumber") number: String, @RequestParam("shopId") shopId: Long): Article {
        return articleService.reserveArticleFromShop(number, shopId)
    }

    @DeleteMapping("/{articleNumber}")
    fun deleteArticle(
        @PathVariable("articleNumber") number: String,
        @RequestParam("shopId", required = true) shopId: Long,
        @RequestParam("reserved", defaultValue = "false") reserved: Boolean
    ): ResponseEntity<Void> {
        articleService.deleteArticleFromShop(number, shopId, reserved)
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
    }
}
