package nl.tnca.intergamma.article

import nl.tnca.intergamma.article.domain.ArticleEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ArticleRepository : CrudRepository<ArticleEntity, Long> {
    fun findByNumberAndShopId(number: String, shopId: Long): List<ArticleEntity>
    fun findByNumber(number: String): List<ArticleEntity>
}
