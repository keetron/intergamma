package nl.tnca.intergamma.article

import nl.tnca.intergamma.article.domain.Article
import nl.tnca.intergamma.article.domain.ArticleEntity
import nl.tnca.intergamma.exception.ArticleUnavailableException
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ArticleService(
    val articleRepository: ArticleRepository
) {
    fun getArticlesByNumberAndShop(number: String, shopId: Long?, available: Boolean): List<Article> {
        return if (shopId == null) {
            articleRepository.findByNumber(number)
                .filter { if (available) !it.isReserved() else it.isReserved() }
                .map { it.toArticle() }
        } else {
            articleRepository.findByNumberAndShopId(number, shopId)
                .filter { if (available) !it.isReserved() else it.isReserved() }
                .map { it.toArticle() }
        }
    }

    fun saveNewArticles(amount: Int, articles: Article): List<Article> {
        val storedArticles: MutableList<Article> = mutableListOf()
        repeat(amount) {
            storedArticles.add(articleRepository.save(articles.toEntity()).toArticle())
        }
        return storedArticles
    }

    fun reserveArticleFromShop(number: String, shopId: Long): Article {
        return articleRepository.findByNumberAndShopId(number, shopId)
            .firstOrNull { it.reservedUntil?.isBefore(LocalDateTime.now()) ?: true }
            ?.copy(reservedUntil = LocalDateTime.now().plusMinutes(5))
            ?.let { articleRepository.save(it) }
            ?.toArticle()
            ?: throw ArticleUnavailableException("No article with number $number available to be reserved.")
    }

    fun deleteArticleFromShop(number: String, shopId: Long, reserved: Boolean) {
        articleRepository.findByNumberAndShopId(number, shopId)
            .firstOrNull { if (reserved) it.isReserved() else !it.isReserved() }
            ?.let { articleRepository.delete(it) }
    }
}

private fun ArticleEntity.isReserved(): Boolean = this.reservedUntil?.isAfter(LocalDateTime.now()) ?: false
