package nl.tnca.intergamma.article.domain

import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity(name = "article")
data class ArticleEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val number: String = UUID.randomUUID().toString(),
    val description: String,
    @ManyToOne(fetch = FetchType.LAZY)
    val shop: Shop,
    val reservedUntil: LocalDateTime? = null
) {
    fun toArticle(): Article {
        return Article(
            number = this.number,
            description = this.description,
            shopId = this.shop.id,
            reservedUntil = this.reservedUntil
        )
    }
}

data class Article(
    val number: String? = UUID.randomUUID().toString(),
    val description: String,
    val shopId: Long?,
    val reservedUntil: LocalDateTime? = null
) {
    fun toEntity(): ArticleEntity {
        return ArticleEntity(
            description = description,
            shop = Shop(id = shopId, name = ""),
            reservedUntil = reservedUntil
        )
    }
}
