package nl.tnca.intergamma

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IntergammaCodeAssessmentApplication

fun main(args: Array<String>) {
    runApplication<IntergammaCodeAssessmentApplication>(*args)
}
