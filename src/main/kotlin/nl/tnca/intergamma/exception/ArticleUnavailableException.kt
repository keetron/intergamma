package nl.tnca.intergamma.exception

class ArticleUnavailableException(message: String) : Throwable(message)
